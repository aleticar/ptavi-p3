from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.root = {}
        self.width = ""
        self.height = ""
        self.background = ""
        self.region = {}
        self.id = ""
        self.top = ""
        self.bottom = ""
        self.left = ""
        self.right = ""
        self.img = {}
        self.src1 = ""
        self.region2 = ""
        self.begin = ""
        self.dur = ""
        self.audio = {}
        self.src2 = ""
        self.begin = ""
        self.dur = ""
        self.textstream = {}
        self.src3 = ""
        self.region3 = ""
        self.etiqueta = ""
        self.content = []

    def startElement(self, name, attrs):

        if name == 'root-layout':
            self.root = {}
            self.root['etiqueta'] = name
            self.width = attrs.get('width', "")
            self.root['width'] = self.width

            self.height = attrs.get('height', "")
            self.root['height'] = self.height

            self.background = attrs.get('background-color', "")
            self.root['background-color'] = self.background
            self.content.append(self.root)

        elif name == 'region':

            self.region = {}
            self.region['etiqueta'] = name
            self.id = attrs.get('id', "")

            self.region['id'] = self.id

            self.top = attrs.get('top', "")
            self.region['top'] = self.top

            self.bottom = attrs.get('bottom', "")
            self.region['bottom'] = self.bottom

            self.left = attrs.get('left', "")
            self.region['left'] = self.left

            self.right = attrs.get('right', "")
            self.region['right'] = self.right
            self.content.append(self.region)

        elif name == 'img':

            self.img = {}
            self.img['etiqueta'] = name
            self.src1 = attrs.get('src', "")
            self.img['src'] = self.src1

            self.region2 = attrs.get('region', "")
            self.img['region'] = self.region2

            self.begin = attrs.get('begin', "")
            self.img['begin'] = self.begin

            self.dur = attrs.get('dur', "")
            self.img['dur'] = self.dur
            self.content.append(self.img)

        elif name == 'audio':
            self.audio = {}
            self.audio['etiqueta'] = name
            self.src2 = attrs.get('src', "")
            self.audio['src'] = self.src2

            self.begin = attrs.get('begin', "")
            self.audio['begin'] = self.begin

            self.dur = attrs.get('dur', "")
            self.audio['dur'] = self.dur
            self.content.append(self.audio)

        elif name == 'textstream':
            self.textstream = {}
            self.textstream['etiqueta'] = name

            self.src3 = attrs.get('src', "")
            self.textstream['src'] = self.src3

            self.region3 = attrs.get('region', "")
            self.textstream['region'] = self.region3
            self.content.append(self.textstream)

    def get_tags(self):
        return self.content


if __name__ == "__main__":
    """
    Programa principal

    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)

    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
