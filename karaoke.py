#!/usr/bin/python3
# -*- coding: utf-8 -*-

from smallsmilhandler import SmallSMILHandler
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request
import json


class KaraokeLocal(ContentHandler):

    def __init__(self, parametro):

        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        self.content = cHandler.get_tags()
        try:
            parser.parse(open(sys.argv[1]))
        except FileNotFoundError:
            print("Usage:python3 karaoke.py file.smil")

    def __str__(self):
        caso = ''

        for impr in self.content:
            for imp in impr:
                if imp == "etiqueta":
                    caso += impr[imp]
                elif impr[imp] != "":
                    caso += "\t" + imp + "=" + impr[imp] + ''
            caso += "\n"

        return (caso)

    def to_json(self, parametro):
        print("Creando archivo .json...")
        json.dump(self.content, open("karaoke.json", "w"), indent=3)

    def do_local(self, parametro):
        for nombre in self.content:
            for parametro in nombre:
                if parametro == 'src' and nombre[parametro].startswith('http'):
                    print("Descargando archivos...")
                    descarga = nombre[parametro].split("/")
                    urllib.request.urlretrieve(nombre[parametro], descarga[-1])


if __name__ == "__main__":

    parametro = sys.argv[1]
    MiKaraoke = KaraokeLocal(parametro)
    MiKaraoke.to_json(parametro)
    MiKaraoke.do_local(parametro)
    print(MiKaraoke)
